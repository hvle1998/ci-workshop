package nl.han.aim.tourofheroes;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Hero {
    private @Id Long id;
    private String name;

    public Hero(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Hero() {}

    public static Hero from(String name) {
        return new Hero(null, name);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
