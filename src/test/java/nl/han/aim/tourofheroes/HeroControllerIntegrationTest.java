package nl.han.aim.tourofheroes;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("integration-test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class HeroControllerIntegrationTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void listOfHeroesIsSixLong() throws Exception {
		mockMvc.perform(get("/api/heroes")
				.accept("application/json"))
				.andExpect(status().isOk())
				.andExpect(content().string("[{\"id\":6,\"name\":\"Happy\"},{\"id\":5,\"name\":\"Lucky\"},{\"id\":4,\"name\":\"Teun\"},{\"id\":3,\"name\":\"Anne\"},{\"id\":2,\"name\":\"Janneke\"},{\"id\":1,\"name\":\"Rody\"}]"));
	}
}
